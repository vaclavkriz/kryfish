const mdsMatrix = [['01', 'EF', '5B', '5B'], ['5B', 'EF', '5F', '01'], ['EF', '5B', '01', 'EF'], ['EF', '01', 'EF', '5B']]
const RSMatrix = [
    ['01', 'A4', '55', '87', '5A', '58', 'DB', '9E'],
    ['A4', '56', '82', 'F3', '1E', 'C6', '68', 'E5'],
    ['02', 'A1', 'FC', 'C1', '47', 'AE', '3D', '19'],
    ['A4', '55', '87', '5A', '58', 'DB', '9E', '03'],

]
const q0Table = [
    [8, 1, 7, 13, 6, 15, 3, 2, 0, 11, 5, 9, 14, 12, 10, 4],
    [14, 12, 11, 8, 1, 2, 3, 5, 15, 4, 10, 6, 7, 0, 9, 13],
    [11, 10, 5, 14, 6, 13, 9, 0, 12, 8, 15, 3, 2, 4, 7, 1],
    [13, 7, 15, 4, 1, 2, 6, 14, 9, 11, 3, 0, 8, 5, 12, 10]
];
const q1Table = [
    [2, 8, 11, 13, 15, 7, 6, 14, 3, 1, 9, 4, 0, 10, 12, 5],
    [1, 14, 2, 11, 4, 12, 3, 7, 6, 13, 10, 5, 15, 9, 0, 8],
    [4, 12, 7, 5, 1, 6, 9, 10, 0, 14, 13, 8, 2, 11, 3, 15],
    [11, 9, 5, 1, 12, 3, 13, 14, 6, 4, 7, 15, 2, 0, 8, 10]
];

const twoFishRound = ([r0, r1, r2, r3], i, keys, mode) => {
    let final = [r0, r1];
    //console.log(r0, r1);
    let [f0, f1] = funcF(r0, r1, i, keys, mode); //VÝSTUPY FUNKCE F
    //console.log(f0, f1);
    let c2 = rotateRight(xor([f0], [r2]).toString(), 1)
    let [c3] = xor([rotateLeft(r3.toString(), 1)], [f1])
    final.unshift(c2, c3)
    //Další runda
    //return prvky do další rundy
    //console.log(final);
    return final;

}
const funcF = (r0, r1, i, keys, mode) => {
    let tempKeys;
    let rR0 = r0;
    let rR1 = rotateLeft(r1.toString(), 8)

    let T0 = funcG(rR0, keys);
    let T1 = funcG(rR1, keys);

    [res1, res2] = PHT(T0.toString(), T1.toString());
    if (mode === 0) {
        tempKeys = [keys.K[31 - i], keys.K[31 - (i + 1)]]
    }
    else {
        tempKeys = [keys.K[2 * i + 8], keys.K[2 * i + 9]]
    }
    const f0 = modSum(res1, tempKeys[0]);
    const f1 = modSum(res2, tempKeys[1]);

    return [f0, f1];
}

const funcG = (r, keys) => {
    const [x0, x1, x2, x3] = divideTo4Bytes(r);
    const [y0, y1, y2, y3] = sBoxes([x0, x1, x2, x3], keys)
    let [r0, r1, r2, r3] = MDS([y0, y1, y2, y3]);
    return [r0, r1, r2, r3].join('');
}

const PHT = (t0, t1) => {
    const res1 = modSum(t0, t1)
    const res2 = modSum(t1, res1);
    return [res1, res2];
}

const sBoxes = ([x0, x1, x2, x3], keys) => {
    let sKeysTemp;
    if (keys.S) {
        sKeysTemp = [keys.S.S0, keys.S.S1]
    }
    else {
        sKeysTemp = keys;
    }
    let XS1 = mixBits([q0(x0), q1(x1), q0(x2), q1(x3)])
    let [XS1xored] = xor([XS1], [sKeysTemp[0]])
    if (XS1xored)
        [x0, x1, x2, x3] = divideTo4Bytes(XS1xored);
    let XS2 = mixBits([q0(x0), q0(x1), q1(x2), q1(x3)])
    let [XS2xored] = xor([XS2], [sKeysTemp[1]])
    if (XS2xored)
        [x0, x1, x2, x3] = divideTo4Bytes(XS2xored);
    return [q1(x0), q0(x1), q1(x2), q0(x3)];

}
const MDS = ([y0, y1, y2, y3]) => {
    const mds = getMDS();
    const Y = [[y0], [y1], [y2], [y3]]
    //console.log(Y)
    const [z0, z1, z2, z3] = multiplyMatrix(mds, Y)
    return [z0, z1, z2, z3]
}

const q0 = x => {
    let [a0, b0] = [x.substring(0, 4), x.substring(4, 8)];
    let a1 = xor2(a0, b0);
    let b1 = xor2(xor2(a0, modMul(a0, 8)), rotateRight([b0], 1))
    let a2 = parseInt(q0Table[0][binToDec([a1])], 10).toString(2).setPadding(4); //t0
    let b2 = parseInt(q0Table[1][binToDec([b1])], 10).toString(2).setPadding(4); //t1
    let a3 = q0Table[2][binToDec([xor2(a2, b2)])]; //t2
    let b3 = xor2(xor2(a2, modMul(a2, 8)), rotateRight([b2], 1))
    let b4 = q0Table[3][binToDec([b3])];
    return parseInt(16 * Number(b4) + Number(a3), 10).toString(2).setPadding(8);
}

const q1 = x => {
    let [a0, b0] = [x.substring(0, 4), x.substring(4, 8)];
    let a1 = xor2(a0, b0);
    let b1 = xor2(xor2(a0, modMul(a0, 8)), rotateRight([b0], 1))
    let a2 = parseInt(q1Table[0][binToDec([a1])], 10).toString(2).setPadding(4); //t0
    let b2 = parseInt(q1Table[1][binToDec([b1])], 10).toString(2).setPadding(4); //t1
    let a3 = q1Table[2][binToDec([xor2(a2, b2)])]; //t2
    let b3 = xor2(xor2(a2, modMul(a2, 8)), rotateRight([b2], 1))
    let b4 = q1Table[3][binToDec([b3])];
    return parseInt(16 * Number(b4) + Number(a3), 10).toString(2).setPadding(8);
}

const subkeyGen = key => {
    const [s0, s1] = subkeySGen(key);
    const kKeys = subkeyKGen(key);
    return {
        S: {
            S0: s0,
            S1: s1,
        },
        K: kKeys,
    }
}

const subkeySGen = key => {
    let sepKey1 = []
    let sepKey2 = []
    for (let i = 0; i < key.length / 8; i++) {
        if (i < 8) {
            sepKey1.push(key.substring(i * 8, i * 8 + 8))
        }
        else {
            sepKey2.push(key.substring(i * 8, i * 8 + 8))
        }
    }
    const mSepKey1 = sepKey1.map(s => [s]);
    const mSepKey2 = sepKey2.map(s => [s]);

    const sKey0 = multiplyMatrix(getRS(), mSepKey1);
    const sKey1 = multiplyMatrix(getRS(), mSepKey2);
    return [sKey0.join(''), sKey1.join('')]
}

const subkeyKGen = key => {
    let sepKey1 = []
    let sepKey2 = []
    let sepKey3 = []
    let sepKey4 = []
    const mEven = []
    const mOdd = []
    let keys = [];
    for (let i = 0; i < key.length / 32; i++) {
        sepKey1.push(key.substring(i * 8, i * 8 + 8))
        sepKey2.push(key.substring(i * 8 + 8, i * 8 + 16))
        sepKey3.push(key.substring(i * 8 + 16, i * 8 + 24))
        sepKey4.push(key.substring(i * 8 + 24, i * 8 + 32))
    }
    mEven.push(sepKey1.join(''), sepKey3.join(''))
    mOdd.push(sepKey2.join(''), sepKey4.join(''))

    for (let i = 0; i < 20; i++) {
        let h0 = funcH(parseInt((2 * i), 10).toString(2).setPadding(8), mEven);
        let h1 = rotateLeft(funcH(parseInt((2 * i + 1), 10).toString(2).setPadding(8), mEven), 8);
        let [k0, k1] = PHT(h0, h1)
        k1 = rotateLeft(k1, 9);
        keys[i * 2] = k0;
        keys[i * 2 + 1] = k1;
    }
    return keys;
}

const funcH = (r, keys) => {
    const [x0, x1, x2, x3] = [r, r, r, r];
    const [y0, y1, y2, y3] = sBoxes([x0, x1, x2, x3], keys)
    let [r0, r1, r2, r3] = MDS([y0, y1, y2, y3]);
    return [r0, r1, r2, r3].join('');
}

const divideTo4Bytes = r => {
    return [r.substring(0, 8), r.substring(8, 16), r.substring(16, 24), r.substring(24, 32)]
}
const divideTo16Bytes = r => {
    return [
        r.substring(0, 8), r.substring(8, 16), r.substring(16, 24), r.substring(24, 32),
        r.substring(32, 40), r.substring(40, 48), r.substring(48, 56), r.substring(56, 64),
        r.substring(64, 72), r.substring(72, 80), r.substring(80, 88), r.substring(88, 96),
        r.substring(96, 104), r.substring(104, 112), r.substring(112, 120), r.substring(120, 128)
    ]
}
const divideTo8Bytes = r => {
    return [
        r.substring(0, 8), r.substring(8, 16), r.substring(16, 24), r.substring(24, 32),
        r.substring(32, 40), r.substring(40, 48), r.substring(48, 56), r.substring(56, 64),
    ]
}

const mixBits = (bits) => {
    return bits.reduce((a, b) => a.toString() + b.toString(), '')
}

const getMDS = () => {
    return mdsMatrix.map(x => {
        return x.map(y => {
            return divideTo4Bytes(hexToBin(y))[3]
        }
        )
    })
}

const getRS = () => {
    return RSMatrix.map(x => {
        return x.map(y => {
            return divideTo4Bytes(hexToBin(y))[3]
        }
        )
    })
}

const galoisMulti = (a, b) => {
    let add = "";
    const G = "101101001";
    let res = [];
    let c = "";

    for (let i = 7, j = 0; i >= 0 && j < 8; i-- , j++) {
        if (Number(b[i]) === 1) {
            res[j] = a;
        }
        else {
            res[j] = "00000000";
        }
        for (let k = 0; k < j; k++) {
            res[j] += 0;
        }
    }
    for (let i = 0; i < 8; i++) {
        c = gal1(res, c, i)
        if (i === 0) {
            i = 1;
        }
    }
    return gal2(c, G);
}

let gal1 = (res, c, i) => {
    if (i === 0) {
        c = xorEnd(res[i], res[i + 1]);
    }
    else {
        c = xorEnd(c, res[i])
    }
    return c;
}

let gal2 = (a, G) => {
    let i = 0;
    while (a.length !== G.length) {
        a = a.split('')
        while (a[i] == 0) {
            a.shift();
            i = 0;
        }
        a = a.join('')
        a = xor2(a, G);
    }
    a = a.substring(1, 9);
    return a;
}

const multiplyMatrix = (mA, mB) => {
    let result = new Array(mA.length);
    for (let i = 0; i < result.length; i++) {
        result[i] = new Array(mB[i].length);
        for (let j = 0; j < mA.length; j++) {
            result[i][j] = 0;
            for (let k = 0; k < mB.length; k++) {
                let gm = galoisMulti(mA[i][k], mB[k][0])
                result[i][j] = xor2(result[i][j], gm)
            }
        }
    }
    return [result[0][0], result[1][0], result[2][0], result[3][0]];
}

const modMul = (bin, dec) => {
    let fuchs = parseInt((dec * binToDec([bin])) % 16, 10).toString(2)
    const add = 4 - fuchs.length;
    fuchs = fuchs.split('');
    fuchs.unshift('0'.repeat(add));
    return fuchs.join('');
}

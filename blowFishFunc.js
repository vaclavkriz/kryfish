//pre-generated sBoxes and pArray
const sbox0 = ['3a432531', '29d3f72e', '5e6e66ad', 'f619cae5', '140f154d', '1e996e5d', '1d3aa0e5', 'b249bfb5', 'af2bebf3', '951d6a26', '8754eee8', '97ab715', '375a6b58', '504acf4', 'e82cafbc', 'e16a95e4', 'fab14d6c', '3c0b83d6', '91d3a2b8', '32e8c6bb', '7f24b93d', '969fcbbf', '50fb617f', '942bd15e', 'dc99dcc3', '3e059863', '11f8c548', '275916cd', '6267cf65', '23e896f1', '2b12ea85', 'eee4336d', '2d1e1ac', '35d0fdea', '3fde8522', '14d54037', '48771ef9', 'fffd2a69', '56d85a8c', '94c68081', '7577040b', '160d197', '9b7b29a', '4f2974cd', 'b3cf4589', '51afeae0', 'd445f5c7', '1668fbb6', '87516bda', 'f61070d0', '49a0aa9f', 'd01705e3', '3b0093d', 'e72183b7', 'cbdbf268', '67985aa1', '8bf6fc09', '59a6aaa8', '57c0410d', 'e4fe84c9', '919b2705', 'b6ff22d1', '7cef788', '9858b289', 'cba010b9', '2acc7f0c', 'f801f83', 'a469b053', '213035f6', '11a9ce5d', '3b68b3bd', 'fed9c78e', '6647be31', '4aef641', '3532d88e', 'c042cda5', '44ea0d1e', 'fd0c3ce7', '751ebd39', '84668a42', '21767955', 'fc94fcdb', 'b4b51afe', '7c090921', '60f1d0b7', 'dab00147', 'fd003cd6', '8eabe5cd', 'ffdf3ce3', '57473143', '38416a6d', 'b0445a30', '7447f652', '8701b339', 'ac8441c4', 'c116b22b', 'df709b7d', '286c7dc9', '7f3548a8', '14a60ca3', 'd4fefff3', '71e47284', '54bf14dc', '5126dc0d', 'd8717e9a', 'de41e838', '510aff66', 'f17827c4', '20c22ed9', '5c7d5d19', '811fb5bd', '77cc9de8', 'c8c931d9', '31a81c98', '7df00880', '36de4237', '2192d745', 'c99fe7d0', 'b57b7d03', '8de38870', '1ddbf9d2', '78c51642', '17e0a433', '1c652fba', '13239590', '7048174d', 'e4102bfc', '533003bf', 'a9225e6f', '4e599eda', 'ee00346', '65546546', '22f45acd', '36c96b8d', 'd19076bc', 'd12866eb', 'cd0141ce', '8aaf011b', '4b66c4f6', '79a6fdb6', 'b17c3769', 'e5de76f1', '4e08122a', '604d15eb', '5b21de43', '6b67c86a', '2c15bd3b', 'c20a1da5', 'bd5e26a1', '9c70e5cd', 'dcf68081', 'ddcad48', 'fbb724a9', '2ef9d53f', '6d0aefb0', '46f0b946', 'a4e83746', '68dde098', '8fe2b05d', '3a11031d', '10516a2f', 'd303743', 'cab4d288', '22d42b32', '16470575', '4baa11b0', '7d1d30c9', '163adf74', '51abfcc2', 'a255487e', '8f978b1a', '2a34a7ff', '47e2be74', 'fa6bd64e', '82bd6528', '36025945', 'b31e3345', '24d55921', 'e9f763a0', '3b757e00', '3824bd1', '8740280', '2adf46d8', 'fa04c4ad', '99e8d042', '6cbd078a', '8497ae2e', '3c084afe', 'd546b6c5', 'beea4279', '4287afd6', '9c7712b1', 'ca13dbdc', '8e9be6ff', '5c6f5aab', '5abe3a2e', 'f8163628', 'd376bf15', 'e45f5ab7', '25b22bb7', 'a6bc9042', '92f5527a', '98224721', '63db88f0', '10da3ce5', '5e9a1583', 'd48e35f4', 'ea03272c', '1122ee81', 'b46f83fe', '1502bd14', 'efa647ef', '3494e065', 'c3e6977b', '8e2bcf7d', '5c9af1e5', 'a335f1dd', 'bc9f0dc4', '19f52ddf', '69db1cfc', 'b3306e31', 'a72a3cc8', '774b04f1', '7b79f77d', '6014f32b', '6465dbe8', 'f85ee2cd', '32fee137', '7ceab847', 'aeb1ce', '3b8bffe0', '5bea24ea', '4d7f566f', 'f59d7b44', '978d4a46', 'e3076dd6', 'e0b543a7', '164f093d', '22905f03', 'cf92ce41', 'ba622b2', '2cee3180', '60a47b37', '9075319d', '77d83a19', 'e9f76004', '4f0689af', 'b530f5fd', '49f34b2d', '428cee87', '96772755', 'f41e613b', '5ec8f816', '71ffa4e0', 'f105a218', '1390fc30']
const sbox1 = ['51f2dcc8', 'cf674a49', 'f6c3bc94', '77580c33', '5839d6a5', '5edd2f92', '4af491f6', 'b63ce943', '4860de33', '7aa0acb0', '9a8ce540', '102d324', '95e82cc3', '999ff507', 'f54689ac', '93c9b0e6', '4a9094af', 'cb495269', 'a08e4a68', 'cc4bb104', '2f45116f', 'e4c51d49', '2ba25809', 'aa884671', '654bdc53', '5edd5ae7', '38ee2a1b', '4033dadc', 'cbcd81e2', '49c52e47', '18646383', '6850ef5', '936c8fb1', 'd1a84691', '7dbc881b', '19059cdb', 'aa1b5fb1', 'ab2663f6', '677ccd7b', '721f958c', '9d7619e0', '626971bf', 'd018718f', '86a84970', '92beae42', '7b8e01d3', 'e7cde21a', '5a9ed32e', '4c6961c0', '9d6109f', 'c4e9fc78', 'faf19b26', 'ab094345', 'f61fc552', '31e1656e', '15fee77b', '6a1539a6', '170b3fa9', '443bf290', '9b176ce4', '6b6f8f52', '6737934d', '99e7648a', '8b637c03', 'ae9a1223', 'f1698f39', '17ebeceb', '8f256434', 'e3c2d9dd', '75727c79', '2e921247', '395f4efa', '1f05895c', 'aece8c0', '303cf36f', '3220bc28', '4bd70706', '1ed68245', '7c67bbc7', '147dd8c3', 'de11afd5', '7c4d6d52', '83a61444', '13711699', 'fe61f17', 'c218139c', '68d9bdea', 'dbb555d', 'bbc944c9', 'b54b1c57', '8b7c5b9e', 'ca56d25d', 'e307cb2e', '1917a687', 'f839b9f3', '6092e681', '30b0cd83', '225cb96b', 'a54072fd', 'f1e79601', 'd97c2503', '9998ab4a', '6555b29c', 'b2931eab', 'c35e2ef2', '28c338c2', '91b00305', '6900053b', 'cff9f2f7', 'f0d3578d', '58b1bec9', '58b03943', 'c16682bc', 'f88f65b2', '47076d82', '8a2a8d51', '8bbafd32', 'ddb85d06', '69338e70', '8581ddef', 'fc2bd445', '3e296108', 'cd968d24', 'a5aa89c1', 'a06798a0', 'c1300393', 'c61641d', '95a881fa', '590177d7', '2dce182f', '11c260f3', '6ad9f266', '8f65169', 'ced37ec4', 'b8034167', '9f26bef', 'a673b5b2', '9bdba597', '43c31cfd', '2485e639', '365e5490', '96ac981b', '6eb0419a', '4054136c', '877770f7', '5ebb5518', 'e3fc21a5', '2da90657', '3bcbeed', 'c0c633a', 'b0034ec6', 'b6bc07b0', '6f36efea', '1bd2d33c', '72b23da9', '7c8160ff', 'f7517a09', '887a96b2', 'f6f83360', '84bfbb57', '1cf13fab', '15f576ae', 'cfd122de', 'c88cd5fe', 'b6e95185', '94158c1', 'b377a6d9', '129fbbd3', '8375a8b1', '5d703684', 'b7376a19', '56723a65', '4d9f692b', 'b63ac461', 'c4cabf', '82a42d96', 'a108bfea', '21773bf7', 'c76f7426', 'e80f96e1', '6073db29', '3994fa5', 'e725790b', '47b07ad4', 'e7fd60a', '31db670c', '8d84f460', '310feac8', '838c5ec5', 'bf7c83de', 'e00ee879', '8828d137', '810e757d', 'db496326', 'f5060a59', '9c902acc', '637aa297', 'fa2387d4', 'e810bc9e', 'ada1dd6d', 'b34ad6aa', '15985fdd', '7b5168b9', 'c0e1e868', 'dd70a719', '846f508f', 'ea5c382e', 'f14381b2', '7cdb0500', '18f687', '62bcbf3c', '28c861e0', '2b5ce935', '6f662aa0', 'ae5fd0c8', 'fd79fa3a', '307ef68a', '2d45a672', '26613c9c', '75fe64a9', 'da6cfe58', 'bf31d7db', 'b492d92b', 'd49d4447', '2cf71400', 'c132cac4', '44874e54', 'dd2c13bb', '3b92dd36', '8809d8e7', 'f2734102', '35333231', '9be7dc86', '278cf537', '99722c15', '1890b40f', 'eb4c6414', '60c81035', '786ec3dd', 'cc522f9a', 'a82761bc', '6375bc4f', '8e92bb04', 'b1105595', '7298c425', 'e68ad58b', 'd1ba4ec4', 'c5595d06', 'dea7a9c7', '4443bc6e', 'fa2caba2', '3dc58eee', '43577197', '61bcb417', '469d1a91', 'b97376f1']
const sbox2 = ['593f901e', 'd14624c9', '7304d2b', '6576af53', 'b3a6f9c6', '9a042d37', '5532c7f0', 'ec33620', 'b6606dd1', '422bcca1', 'd24e9206', 'b592c560', '26400044', 'e8f2281c', 'b46dc674', 'ba35947', 'c8304cc9', 'd366dc11', '34d9f462', '6874b442', 'f21739c', '1eb6d4e8', '5d2793dc', 'c8098be', '39491e5a', '2bf2e063', '6addd6ea', '58c354d2', 'a78400f4', 'c9d7be70', 'd81980f3', 'e80ed93f', 'f8776a64', '42775b9', 'efa804bd', '7e5fe3a7', '9b06ad64', 'b89b38ab', 'f6fba808', 'c1b191c6', 'aa04971c', 'd325fc42', '1a9807bd', '45f8e7d', '87a86769', 'fb8e1e32', '2c944ac0', 'e2d0ecba', '52885533', '735fafd3', '8cdbac2e', '9d1d1576', '679762a', '898de37', '1c7924a3', 'ad908477', '13469040', 'd836295d', '8d66699a', '23ed8ee0', '8fc344c3', '89acb40f', '446e2ffd', 'e7a1567b', '1f67efce', 'db1c04d1', 'ebcd4d79', '691109d6', '3c4b7e35', '69fb900e', '3b06b05', 'ac635466', '64ec12be', '6b4a867', '15abfecb', 'ab27c072', '74b0fbf6', 'da0839fa', '4e8af59d', 'b5048162', '4ad60ac8', '78255ee8', '150006ed', '31be3d9f', '4ec71290', 'be111238', 'c3f22b3c', 'e4f052da', 'bfe04034', 'be0a25f5', '7a22c558', '943fc75', '3d4f9f63', '7089cff', '887f9963', 'cbeb32b7', 'ad96c968', '9ad7dad4', 'e38bd6e2', '2373aaec', 'f8086dc9', 'c513caf4', '9cb3ca65', '4ce8ece3', '43ae0be3', 'f394a0b0', 'b77b2cbe', '9cec5bf0', '209bf8b2', '6345bbf5', '78394674', 'ddbb2e63', '53f371ff', 'd7179d11', '2dc4e141', '91942c90', 'b9cac128', '5bf3dacc', '3dace452', 'a3dd7822', 'df891b94', 'd6d1e3da', 'b924119b', 'e5222921', 'c913caf8', 'd52d5f49', 'a799e08b', 'f23749a7', 'c5f4ecd1', 'aefedb9', '9edda2ca', '8c6e0798', '44e354c5', 'e441b6d', '6b777a2d', 'fdbfc4da', '62f3a94f', '6062c78f', 'de6c775b', '7fab464', '62db2c4c', 'be0c2cb0', 'ea9101bb', 'c932dd8c', '4e7142d7', '69fa73c2', '747dc05d', '57bc9992', '4581c9ab', 'e0ce3145', '12164be', 'fedf8d34', 'f87e03aa', 'c58b4e92', '76c3e803', 'bf4ebd65', 'd569df67', '30f716ec', '77f0c1b6', 'bb5f1988', 'b3ca5789', 'db6fde6d', 'de971e7a', '8c8e7c94', '28e9f15e', 'bd012f10', 'ba29e478', '1b06f61a', '291726d8', '80c9922f', '41984b14', 'b206deaf', '3dba4989', 'ec571e0e', 'bb7e14ad', 'e75d22e7', '47dffa8a', '1dee8257', '7bd66b4c', 'c2bbf2cc', '6cf8e16a', '1c503f05', '2356ed0b', 'e43d98b1', '622891c3', '9cf5e2c0', 'd8d7c59c', 'e6fcba49', '42f91ae', 'ec1aa5a', '36477e9d', '7c0b0e41', 'ee375e9a', '55473d55', '3d167c16', 'fbb3cfd8', '962eca9e', '8eb34c1c', '191bde48', '912cae89', 'f7c33c02', 'a37b6502', '933a049', 'a06337fb', '57fb97b5', 'eda29b0', 'f0a90143', '7c558d96', '48cced4b', '75c26f50', 'a9690cb6', '8fc18d76', '5cd0e2fd', '47d6c8d6', '1b60730f', 'c00e5173', '64105678', '7e1f1da1', '7a7c896c', '8fcdb69b', '1db79a08', 'ae6e6895', '1bb280f2', '2805f77a', '649aa449', '8a0742c3', 'd20332e1', '12c59be5', '43ef47ba', 'fc327002', 'd840f98', '1255c1e7', 'c7b15f33', 'eee4b891', 'ad8e0041', '93b49ec2', '15ecca6b', 'b534814e', 'dd18cbfc', 'b4937fd5', '8db9ee83', '1fa5947', 'efbf6653', 'aa5c9e85', 'f6e8060d', '4a5a105a', '7c14c45', 'ecead582', '85559fb2', 'b13155ed', '23d164cb', 'abc8b734', '24d47f28', '7a79661f', '180381cb', '63be4f57']
const sbox3 = ['e1a79482', 'c661fddd', 'f3fca515', '6f49c92a', '31465562', 'be79f28b', 'b0a4902', 'b2201d30', '86c78e6b', '15eae4b0', 'ed772d4d', '622c3397', '83f32cd', '581c2c6d', 'c9abc26f', '11c92f62', 'f5bc3579', '416e661a', '9ecaeacb', 'c76bc125', '9a9d2457', '78acc87', '16a47202', '7d8e616', 'd2f63965', 'a78f1b94', '82f5fe7c', '79536785', 'b89ce2db', '11cb1404', '91724478', '3ea7d993', 'ff884394', '8b7cf103', 'bc7d497b', 'd250507c', 'fd0155e1', '9f4057cd', '28cc03d4', '11a80269', '9c777062', '84af47cb', 'f4c87962', '54cad645', 'ff32f5eb', '71b43bc', '6602b89d', '18a4fa56', '390fecf2', '2e65dd2b', 'c429ce11', '64546b23', 'debfe45a', 'c00d315', '100e2f76', 'ae0edcbf', 'e63d66fd', 'cc37c07c', '5b3f07e7', '8c9383d1', '6bd86c8', '654b7820', '794a932c', '5d2dddb9', 'f2475992', 'd7d12f93', '1507d74e', 'a7861e1', '2114595e', '28818e4d', '9cb63135', '16b7a6bf', '96454f70', 'b17c4ca1', '1be51714', 'd4c1ce96', 'cd086dba', '4567ec76', '1a53d5f8', 'def9dfc8', 'b1a73e38', '3be80566', '21b05d85', 'd90a8fc9', '2a2d8613', '863c92dd', 'f23e16ab', '46b2fdfe', 'b1a88d0b', '3a654e8f', '3afa1297', 'a063a506', '3466a40', '32d098cd', '559af17', 'eed31df2', '1ae85441', '4775ec17', '85a5c58d', '3ce48f02', 'cf0abb7a', '5a3b798e', '473d8785', '9394f22f', 'e90e0f00', 'fa798a7b', '1e84ab80', '9848eb40', '7681960', 'fe63395c', 'cc30258f', 'b86296d4', 'a55707eb', '67755335', 'acfc7ab0', 'e5192d12', 'd10baeb9', '56064084', '8bbd4c1e', '4834323f', '5e47f7df', 'e39fab14', 'd26cfa12', '8341d449', '29406f1c', 'ef5a33b7', '21c6a549', 'c06dbdd2', '4e9cb89d', 'a26f8ece', 'f480b7b2', '5bb1a964', 'e84d5be0', '351402', '7c5deaa8', 'b774f6fc', 'f16d5462', 'f33f1b70', 'ea94c011', '1f5a690b', '9de1f995', '6fb8f417', '1a52dd6e', '663c7def', '6ddf207d', '3051a64b', '81797f5c', 'cebef7b1', '500c5818', '6faf2c50', '50151198', '24b7a4e2', '7e8b65d3', '6a52ba5', 'c2766304', '6e3d6749', '505a615f', '8d395070', 'cca84ca0', '90992641', 'b182bbb4', '829608c', '565d24ed', 'ff546894', 'a73095bb', '6d53cb7e', 'bf8cb0b5', '27effb09', '761c16cc', '2a28bacc', 'f36af1a5', 'e540bcc3', 'd6b061b5', '8684f7d1', '23d82255', 'bbbf1759', '73952657', 'a9a0df8c', '608449cd', '61b20fe6', '57a3db3c', '494353e2', '9ed39620', 'c883c02b', '6e646ccb', '614c244a', '1dfba008', 'd5a2a9fa', 'f71d34b2', 'a823bc36', 'fcaf0101', '40ef0056', '9e8f2d6e', '1337fa8b', '72adc538', '3ffeaa89', 'dfe84c47', '7ee42782', 'd8a588f8', '4186d0e5', 'ca6fe156', '5aea105a', '83c8c005', '6780c419', 'f3779f14', 'a6334572', 'c02b2c91', 'ad2d33c0', '7610d5cf', '38b1eafe', '667a1d3c', 'ee58cd4d', '6d6d1c8f', 'b773471f', 'ee697246', 'fc69c7b3', '4d20131b', 'e22f1433', '54245c5a', '3fad2303', 'ef7e5f5a', '2f662660', '15e3b60d', '553b299e', '4d61df72', '9a93dcbb', 'ef6ba6e3', 'fe337ba2', 'a34d5177', '36d6cfc0', '919e656a', 'cc93dc4c', 'a8627d95', 'f59c7ca0', '47c2b450', '21891e19', '8a30ceb4', '6bba7b2d', '97aa75cd', '5cd68d42', 'de854d2d', 'b15114bc', 'd9b22995', '50a6e169', '2c62ee0', '76c310f3', '7cefca51', '22fefa3f', '593326bc', 'c516f707', 'c93766b9', '116db878', 'ef47fab3', '2e29c56d', '7d653cf7', '323afe05']
const parray = ['2dd2dc1f', '2c7173ca', '4839d47f', '61a6d530', 'd6bb1740', '1b0b0dfd', 'a9761b47', 'b74bce8', '1b57393b', 'dc730b5b', 'f303b073', '5fcb0d3f', 'b8b7dab2', '61c3232b', '5b3b8100', 'a2c7f092', '3751b709', '3d07cfa5']

//initial set of sBoxes and pArray
const generateRandom256Hexs = () => {
    let a = 0;
    let arr = []
    for (let i = 0; i < 18; i++) {
        for (let j = 0; j < 32; j++) {
            a += Math.round(Math.random()).toString();
        }
        document.querySelector('#blowFishOutput').append(`'${binToHex(a)}',`)
        a = 0;
    }
}
//sBox function
const sBox = xL => {
    const sepXL = [xL.substring(0, 8), xL.substring(8, 16), xL.substring(16, 24), xL.substring(24, 32)]
    let keyIndexes = binToDec(sepXL);
    let skeys = [hexToBin(sbox0[keyIndexes[0]]), hexToBin(sbox1[keyIndexes[1]]), hexToBin(sbox2[keyIndexes[2]]), hexToBin(sbox3[keyIndexes[3]])];
    let c0 = modSum(skeys[0], skeys[1]);
    let c1 = xor([c0], [skeys[2]]);
    return modSum(c1, skeys[3]);
}
//preparing key for XORing with pArray
const keyPrep = key => {
    let keyArr = []
    for (let i = 0; i < Math.ceil((32 * 18) / key.length); i++) {
        keyArr += key;
    }
    return keyArr;
}
//modifying output value into valid input
const modifyTempCT = tempCT => {
    tempCT = tempCT.join('');
    tempCT = [tempCT.substring(0, 8), tempCT.substring(8, 16), tempCT.substring(16, 24), tempCT.substring(24, 32),
    tempCT.substring(32, 40), tempCT.substring(40, 48), tempCT.substring(48, 56), tempCT.substring(56, 64)];
    return tempCT;
}
//key XORing with pArray
const keyXorParray = keyArr => {
    let tempArr;
    let newArr = [];
    for (let i = 0; i < parray.length; i++) {
        tempArr = hexToBin(parray[i]);
        for (let j = 0; j < 32; j++) {
            newArr[j] = tempArr[j] ^ keyArr[i * 32 + j];
        }
        parray[i] = binToHex(newArr.join(''));
    }
}
//key schedule
const updateSubkeys = () => {
    let tempCT = mapToBinar("aaaaaaaa");
    tempCT = updateSubkeyArrays(parray, tempCT);
    tempCT = updateSubkeyArrays(sbox0, tempCT); 
    tempCT = updateSubkeyArrays(sbox1, tempCT);
    tempCT = updateSubkeyArrays(sbox2, tempCT);
    tempCT = updateSubkeyArrays(sbox3, tempCT);
}
//updating pairs of subkeys
const updateSubkeyArrays = (array, tempCT) => {
    for (let i = 0; i < array.length; i = i + 2) {
        tempCT = BlowEncrypt(tempCT);
        array[i] = binToHex(tempCT[0]);
        array[i + 1] = binToHex(tempCT[1]);
        tempCT = modifyTempCT(tempCT);
    }
    return tempCT;
}

const BlowEncrypt = binar => {
    let ct = [];
    let xL = '';
    let xR = '';
    //iteration for each block
    for (let b = 0; b < binar.length / 8; b++) {
        for (let j = 1; j <= 4; j++) {
            xL += binar[(b * 8) + j - 1].toString();
            xR += binar[(b * 8 + 4) + j - 1].toString();
        }
        //rounds for each block
        for (let i = 0; i < 16; i++) {
            [xL] = xor([xL], [hexToBin(parray[i])])
            let fRes = sBox(xL);
            [xR] = xor([xR], [fRes]);
            [xL, xR] = [xR, xL];
        }
        [xL, xR] = [xR, xL];
        [xRr] = xor([xR], [hexToBin(parray[16])]);
        [xLl] = xor([xL], [hexToBin(parray[17])]);
        xL = '';
        xR = '';
        ct.push(xLl, xRr);
    }
    return ct;
}

const BlowDecrypt = (binar) => {
    parray.reverse();
    let ot = [];
    let xL = '';
    let xR = '';
    //iteration for each block
    for (let b = 0; b < binar.length / 8; b++) {
        for (let j = 1; j <= 4; j++) {        
            xL += binar[(b * 8) + j - 1].toString();
            xR += binar[(b * 8 + 4) + j - 1].toString();
        }
        //rounds for each block
        for (let i = 0; i < 16; i++) {
            [xL] = xor([xL], [hexToBin(parray[i])])
            let fRes = sBox(xL);
            [xR] = xor([xR], [fRes]);
            [xL, xR] = [xR, xL];
        }
        [xL, xR] = [xR, xL];
        [xRr] = xor([xR], [hexToBin(parray[16])]);
        [xLl] = xor([xL], [hexToBin(parray[17])]);
        xL = '';
        xR = '';
        parray.reverse();
        ot.push(xLl, xRr);
    }
    return ot;
}
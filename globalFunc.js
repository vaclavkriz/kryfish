export const alphabet = {
    'a': '00000000',
    'b': '00000001',
    'c': '00000010',
    'd': '00000011',
    'e': '00000100',
    'f': '00000101',
    'g': '00000110',
    'h': '00000111',
    'i': '00001000',
    'j': '00001001',
    'k': '00001010',
    'l': '00001011',
    'm': '00001100',
    'n': '00001101',
    'o': '00001110',
    'p': '00001111',
    'q': '00010000',
    'r': '00010001',
    's': '00010010',
    't': '00010011',
    'u': '00010100',
    'v': '00010101',
    'w': '00010110',
    'x': '00010111',
    'y': '00011000',
    'z': '00011001',
    '0': '00011010',
    '1': '00011011',
    '2': '00011100',
    '3': '00011101',
    '4': '00011110',
    '5': '00011111',
    '6': '00100000',
    '7': '00100001',
    '8': '00100010',
    '9': '00100011',
}

const getKeyByValue = (object, value) => {
    return Object.keys(object).find(key => object[key] === value);
}
//add characters to OT to be equal to block size
const checkDiv = (input, x) => {
    let addLength = x - input.length % x;
    input += 'a'.repeat(addLength);
    return input;
}
//toLower + trim
const toLowerTrim = input => {
    return input.toLowerCase().replace(/ /g, '');
}
//map OT to binar
const mapToBinar = ot => {
    const binar = ot.split('').map(char => {
        return alphabet[char]
    })
    return binar;
}
//transfering
const binToHex = bin => {
    return parseInt(bin, 2).toString(16);
}

const binToDec = binar => {
    return binar.map(bin => {
        return parseInt(bin, 2).toString(10);
    })
}

const hexToBin = hex => {
    const bin = parseInt(hex, 16).toString(2).split('');
    const addLength = bin.length % 32 !== 0 ? 32 - (bin.length % 32) : 0;
    for (let i = 0; i < addLength; i++) {
        bin.unshift(0);
    }
    return bin.join('');
}
//XOR for arrays
const xor = (block, keys) => {
    let rarray = [];
    let r = '';
    for (let i = 0; i < block.length && i < keys.length; i++) {
        for (let j = 0; j < keys[i].length && block[i].length; j++) {
            r += (block[i][j] ^ keys[i][j]).toString();
        }
        rarray.push(r.toString());
        r = '';
    }
    return rarray;
}
//XOR for strings
const xor2 = (firstInput, secondInput) => {
    let temp = [];
    let [shorter, longer] = whoIsLonger(firstInput, secondInput);
    const add = longer.length - shorter.length;
    for (let i = 0; i < add; i++) {
        shorter += '0';
    }
    for (let i = 0; i < longer.length; i++) {
        if (shorter[i] == longer[i] || shorter[i] === "") {
            temp[i] = '0';
        }
        else {
            temp[i] = '1';
        }
    }
    return temp.join('');
}
//XOR from end
const xorEnd = (firstInput, secondInput) => {
    let temp = [];
    let [shorter, longer] = whoIsLonger(firstInput, secondInput)
    const add = Math.abs(firstInput.length - secondInput.length);
    shorter = shorter.split('');

    for (let i = 0; i < add; i++) {
        shorter.unshift('0');

    }
    shorter = shorter.join('');
    for (let i = longer.length - 1; i >= 0; i--) {
        if (shorter[i] == longer[i]) {
            temp[i] = '0';
        }
        else {
            temp[i] = '1';
        }
    }
    return temp.join('');
}

const whoIsLonger = (a, b) => {
    let shorter = ""
    let longer = ""

    if (a.length <= b.length) {
        shorter = a;
        longer = b;
    }
    else {
        longer = a;
        shorter = b;
    }
    return [shorter, longer]
}

const modSum = (s1, s2) => {
    const bin = (((parseInt(s1, 2) + parseInt(s2, 2)) % (Math.pow(2, 32))).toString(2)).split('');
    const addLength = bin.length % 32 !== 0 ? 32 - (bin.length % 32) : 0;
    for (let i = 0; i < addLength; i++) {
        bin.unshift(0);
    }
    return bin.join('');
}
//get text from cipher binar
const binToText = (str, a, b) => {
    let binarNoPadding = (Number(parseInt(str.substring(a, b), 2)) % 36).toString(2).split('');
    const binarNoPaddingLength = binarNoPadding.length;
    const binarNoPaddingAdd = 8 - binarNoPaddingLength;

    for (let i = 0; i < binarNoPaddingAdd; i++) {
        binarNoPadding.unshift(0);
    }

    return binarNoPadding.join('');
}
//tady bude problem u decryptu
const blockToST = array => {
    return array.map(str => {
        return [
            getKeyByValue(alphabet, binToText(str, 0, 8)),
            getKeyByValue(alphabet, binToText(str, 9, 16)),
            getKeyByValue(alphabet, binToText(str, 17, 24)),
            getKeyByValue(alphabet, binToText(str, 25, 32)),
        ].join('')
    })
}

//rotations
const rotateLeft = (array, num) => {
    let newArray = [];
    for (let i = 0; i < array.length; i++) {
        newArray[i] = array[((array.length) + (i + num)) % array.length];
    }
    return newArray.join('');
}
const rotateRight = (array, num) => {
    let newArray = [];
    for (let i = 0; i < array.length; i++) {
        newArray[i] = array[((array.length) + (i - num)) % array.length];
    }
    return newArray.join('');
}

String.prototype.setPadding = function (num) {
    const length = this.length;
    if (length <= num) {
        const add = num - length;
        let newString = this.split('');
        newString.unshift('0'.repeat(add))
        return newString.join('')
    }
}